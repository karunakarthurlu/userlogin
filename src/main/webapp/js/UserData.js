$(function() {
    var User = Backbone.Model.extend({
    	
        urlRoot : "getuserdata"
    });
   
    var UserColletion=Backbone.Collection.extend({
    	model:User
    });
         
    var TableView = Backbone.View.extend({
    	
        el:"#someElement",
        model:new User(),
        template : _.template($("#rowTemplate").html()),
        initialize: function () {
        	this.render();
		},
		render : function() {
			var that = this;
			$.get('./templates/UserData.html', function(data) {
				template = _.template(data, {});
				$("#regID").empty();
				$("#loginID").empty();	
				that.$el.html(template);
			}, 'html');
			this.$el.html();
			return this;
		},
        events : {
            "click #userdata" : "addMe"
            	
        },
        addMe : function(event) {
        	var that =this;
        	$('#loginID').empty();
        	this.undelegateEvents();
            this.model.fetch({
                success:function (response) {
                	
                	 that.$el.append(that.template({data:response.attributes}));
                	 $(this.el).empty();
                	 $("#rowTemplate").empty();
                }
            });
            
        },
        cleanup: function() { 
            this.$('#userdata').undelegateEvents();
            $(this.el).empty();
            
        }
           
    });
 
    var tableview = new TableView();
    
});