$(function(){
	
	_.extend(Backbone.Validation.callbacks, {
	    valid: function (view, attr, selector) {
	        var el = view.$('[name=' + attr + ']'), group = el.closest('.form-group');
	        
	        group.removeClass('has-error');
	        group.find('.help-block').html('').addClass('hidden');
	    },
	    invalid: function (view, attr, error, selector) {
	        var el = view.$('[name=' + attr + ']'),  group = el.closest('.form-group');
	        
	        group.addClass('has-error');
	        group.find('.help-block').html(error).removeClass('hidden').css("color","red");
	        
	    }
	});
	
	// Model
	var User = Backbone.Model.extend({
		urlRoot : "getUser",
		validation:
		{
			userName: {
				required: true
			},
			userPwd: {
				required: true
			}
		}
	}); 

	//View
	var UserView = Backbone.View.extend({
		model :new User(),
		el:'.container',
		initialize: function () {
			Backbone.Validation.bind(this);
			this.render();
		},
		events : {
			"click #login" : function(e){
				e.preventDefault();
	            this.signUp();
			}
		},
		render : function() {
			var that = this;	
			$.get('./templates/login.html', function(data) {
				template = _.template(data, {});
				$("#regID").empty();
				$("#myTable").empty();
				that.$el.append(template);
			}, 'html');
			this.$el.html();
			return that;
			
		},
		signUp : function() {
			
			var serialized = $('form').serializeArray();
			var s = '';
			var data = {};
			for (s in serialized) {
				data[serialized[s]['name']] = serialized[s]['value']
			}
			var result = JSON.stringify(data);
			var finaldata = JSON.parse(result);
			this.model.set(finaldata); 
			if(this.model.isValid(true)){
				
				this.model.save({},{
					dataType:"json",
					data : $.param({userName:finaldata.userName,userPwd:finaldata.userPwd}),
					success : function(response) {
						if(response.attributes.success == true){
							$('.successdiv').html(" Login successfully done ").css("color","green");
							$('#userName').val('');
							$('#userPwd').val('');			
						}else{
							$('.successdiv').html("Invalid UserName & Password").css("color","red");
							$('#userName').val('');
							$('#userPwd').val('');
						} 
					}			
				});
			}	
		}
	}); 
	
	$(function () {
	    var view = new UserView({el: '#someElement', model: new User() });
	});
});
