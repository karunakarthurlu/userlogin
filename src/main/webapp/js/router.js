var AppRouter = Backbone.Router.extend({
	routes : {
			"register" : "registerView",
			"userdata" : "userdataView",
			"login"    : "indexView"
			},
	registerView : function() {
		require([ "./js/register" ], function() {
		});
	},
	userdataView: function(){
		require([ "./js/UserData" ], function() {
		});
	},
	indexView: function(){
		require([ "./js/Login" ], function() {
			
		});
	}

});
var appRouter = new AppRouter();
Backbone.history.start();
