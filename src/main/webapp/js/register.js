$(function() {
	
	_.extend(Backbone.Validation.callbacks, {
	    valid: function (view, attr, selector) {
	        var $el = view.$('[name=' + attr + ']'), 
	            $group = $el.closest('.form-group');
	        
	        $group.removeClass('has-error');
	        $group.find('.help-block').html('').addClass('hidden');
	    },
	    invalid: function (view, attr, error, selector) {
	        var $el = view.$('[name=' + attr + ']'), 
	            $group = $el.closest('.form-group');
	        
	        $group.addClass('has-error');
	        $group.find('.help-block').html(error).removeClass('hidden').css("color","red");
	        
	    }
	});
	var User = Backbone.Model.extend({
		urlRoot : "saveUser",
		validation:
		{
			userName: {
				required: true
			},
			userEmail: {
				required: true,
				pattern: 'email'
			},
			userPwd: {
				required: true,
				minLength: 4
			}
		}
	}); 
	
	var UserView = Backbone.View.extend({
		model :new User(),
		el:'.container',
		initialize: function () {
			Backbone.Validation.bind(this);
			this.render();
		},
		render : function() {
			var that = this;
			
			$.get('./templates/register.html', function(data) {
				template = _.template(data, {});
				$("#loginID").empty();
				$("#myTable").empty();
				that.$el.append(template);
			}, 'html');
			this.$el.html();
			return this;
			
		},
		events : {
			"click #button" : function(e){
				e.preventDefault();
	            this.signUp();
			}
		},		
		signUp : function() {
			
			var serialized = $('form').serializeArray();
			var s = '';
			var data = {};
			for (s in serialized) {
				data[serialized[s]['name']] = serialized[s]['value']
			}
			var result = JSON.stringify(data);
			var finaldata = JSON.parse(result);
			this.model.set(finaldata); 
			if(this.model.isValid(true)){
				
				this.model.save({},{
					dataType:"json",
					data : $.param({
						userName:finaldata.userName,
						userEmail:finaldata.userEmail,
						userPwd:finaldata.userPwd
					}),
					success : function(response) {
						if(response.attributes.success == true){
							$('.successdiv').html(" registered successfully ").css("color","green");
							$('#userName').val('');
							$('#userEmail').val('');
							$('#userPwd').val('');
										
						}else{
							$('.successdiv').html("user alredy existed").css("color","red");
						} 
					}			
				});
			}	
		}
	});
	$(function () {
	    var view = new UserView({ el: '#someElement', model: new User() });
	});
});
