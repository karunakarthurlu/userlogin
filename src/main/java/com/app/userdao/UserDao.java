package com.app.userdao;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.app.model.User;
import com.app.model.UserRowMapper;

@Repository
public class UserDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@SuppressWarnings("unchecked")
	public String saveUser( String userName,  String userPwd, String userEmail) 
	{
		JSONObject result=new JSONObject(); 
		String sql = " insert into user values(?,?,?)"; 
		int a=jdbcTemplate.update(sql,userName,userPwd,userEmail); 
		if(a==1) {
			result.put("success", true); 
		}else 
		{ 
			result.put("success", false); 
		}

		return result.toJSONString();
	}

	public List<User> getUserData() 
	{
		String sql=" select * from user "; 
		List<User> user=jdbcTemplate.query(sql,new UserRowMapper());

		return user;
	}

	@SuppressWarnings("unchecked")
	public  String getUser( String userName, String userPwd) {

		JSONObject result=new JSONObject(); 
		String sql="select * from user where userName=?";
		User userfromdb=new User();
		try {
			userfromdb=jdbcTemplate.queryForObject(sql,new UserRowMapper(),userName);
		}
		catch(Exception e) {
			result.put("success",false); 
		}

		if(userName.equals(userfromdb.getUserName())&&userPwd.equals(userfromdb.getUserPwd())) {
			result.put("success", true); 
		}
		else {
			result.put("success",false); 
		}
		return result.toJSONString();
	}

}
