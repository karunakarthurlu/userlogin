package com.app.UserService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.model.User;
import com.app.userdao.UserDao;

@Service
public class UserService {
	
	@Autowired
	private UserDao dao;
	
	public  String saveUser( String userName,  String userPwd, String userEmail) 
	{
		return dao.saveUser(userName, userPwd, userEmail);
	}
	public List<User> getUserData() 
	{
		return dao.getUserData();
	}
	
   public String getUser(String userName, String userPwd) {
		return dao.getUser(userName, userPwd);
		
	}

	
}
