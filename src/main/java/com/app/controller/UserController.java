package com.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.UserService.UserService;
import com.app.model.User;

@Controller
public class UserController {

	@Autowired
	private UserService service;

	@RequestMapping(value="/saveUser",method=RequestMethod.POST)
	@ResponseBody
	public  String saveUser(@RequestParam String userName, @RequestParam String userPwd,@RequestParam String userEmail) {
		return service.saveUser(userName, userPwd, userEmail);
	}

	@RequestMapping("/getuserdata")
	@ResponseBody 
	public List<User> getUserData() {
		return service.getUserData();
	}

	@RequestMapping(value="/getUser",method=RequestMethod.POST)
	@ResponseBody 
	public String getUser(@RequestParam String userName, @RequestParam String userPwd) {

		return service.getUser(userName, userPwd);
		
		
	}


}
